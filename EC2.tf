provider "aws" {
  region = "ap-northeast-2"

}

resource "aws_key_pair" "ec2-key-pair" {
  key_name   = "ec2-key"
  public_key = file("./test.pub")

}


resource "aws_instance" "test-ec2" {
  ami           = var.ami-AmazoneLinux2
  instance_type = "t2.micro"
  key_name      = aws_key_pair.ec2-key-pair.key_name
  user_data     = file("./install_docker_packer.sh")
  tags = {
    Name = "EC2-TEST"
  }
}

